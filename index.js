const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

// allows backend to be available to frontend
// allows us to control the app's Cross Origin Resource Sharing Settings
const cors = require("cors");

const port = 4000;
const app = express();

// Connect to mongoDB database
mongoose.connect("mongodb+srv://admin:admin123@course-booking.qxq86.mongodb.net/s32-s36?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', () => console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Now connected to MongoDB Atlas'));


// allows all resources to access backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// will use the defined port number for the application whenever an environment variable is available OR will use port 4000 if none is defined
// this syntax will allows flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online in port ${process.env.PORT || port}`);
});

