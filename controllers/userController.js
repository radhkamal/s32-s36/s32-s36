const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../jwt/auth")


// Check if email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

// User registration
module.exports.registerUser = (reqBody) => {


	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNumber: reqBody.mobileNumber,
		isAdmin: reqBody.isAdmin,
		// 10 is the value provided as the number of "salt" rounds that the bycrypt algorithm will run in order to encrypt the password
		// hashSync(<dataToBeHashed><salt>)
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if (error) {
			return false
		} else {
			return true
		}
	})
}

// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {

			// compareSync(<dataToBeCompared>, <encryptedData>)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
}


// 2. Create a getProfile controller method for retrieving the details of the user:
// a. Find the document in the database using the user's ID
// b. Reassign the password of the returned document to an empty string
// c. Return the result back to the frontend
// User retrieval
module.exports.retrieveUser = (reqBody) => {
	return User.findOne({_id: reqBody.id}).then(result => {

		if (result == null) {

			return false;

		} else {

			result.password = "";
			return result;
		}

	})
}

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {

		result.password = "";
		return result;

	})
}

// Enroll user to a class
module.exports.enroll = async (data) => {

	if (data.isAdmin == false) {

		let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});

			return user.save().then((user, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			})
		})

		let isCourseUpdated = await Course.findById(data.courseId).then( course => {
			course.enrollees.push({userId: data.userId});

			return course.save().then((course, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			})
		})

		if (isUserUpdated && isCourseUpdated) {
			return true;
		} else {
			return false;
		}

	} else {
		return `Only regular users can enroll. Please log in as a regular user`
	}
}

