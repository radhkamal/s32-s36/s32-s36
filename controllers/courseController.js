const Course = require("../models/Course");

module.exports.addCourse = (reqBody) => {

	let newCourse = new Course ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {

		if(error) {
			return false;
		} else {
			return course;
		}
	})
}


/*


Activity:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Push to git with the commit message of Add activity code - S34.
4. Add the link in Boodle.


*/

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

module.exports.getActiveCourses = (reqBody) => {
	return Course.find({isActive: true}).then(result => {
		if (result == null) {

			return false;

		} else {

			return result;
		}
	})
}

// updating a course
module.exports.updateCourse = (data) => {
	console.log(data);
	return Course.findById(data.courseId).then((result, error) => {
		console.log(result);

		if (data.isAdmin) {
			result.name = data.updatedCourse.name;
			result.description = data.updatedCourse.description;
			result.price = data.updatedCourse.price;

			console.log(result);

			return result.save().then((updatedCourse, error) => {
				if (error) {
					return false
				} else {
					return updatedCourse
				}
			})
		} else {
			return "Not admin."
		}
	})
}

// Instructions:
// 1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
// 2. Create a controller method for archiving a course obtaining the course ID from the request params
// 3. Process a PUT request at the /courseId/archive route using postman to archive a course
// 4. Push to git with the commit message of Add activity code - S35.
// 5. Add the link in Boodle.

module.exports.archiveCourse = (data) => {
	console.log(data);
	return Course.findById(data.courseId).then((result, error) => {
		console.log(result);

		if (data.isAdmin) {
			result.isActive = false;

			console.log(result);

			return result.save().then((result, error) => {
				if (error) {
					return false;
				} else {
					return result;
				}
			})
		} else {
			return "Not admin."
		}
	})
}