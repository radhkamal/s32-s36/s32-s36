const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../jwt/auth");

// Route for creating a course
// router.post("/", (req, res) => {

// 	courseController.addCourse(req.body).then(resultFromController => {
// 		res.send(resultFromController);
// 	})
// })

// Route for creating a course with admin authentication
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if (userData.isAdmin == true) {
		courseController.addCourse(req.body).then(resultFromController => {
			res.send(resultFromController);
		})
	} else {
		return false;
	}
})

module.exports = router;


/*


// Route for User Details
router.post("/details", (req, res) => {
	userController.retrieveUser(req.body).then(resultFromController => {
		res.send(resultFromController);
	})
})


router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.getProfile({userId: userData.id}).then(resultFromController => {
		res.send(resultFromController);
	})
})

*/

/*


Activity:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Push to git with the commit message of Add activity code - S34.
4. Add the link in Boodle.


*/

// Route for Retrieving all the courses
router.get("/all", auth.verify, (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

// create a route and controller for retrieving all active courses

router.get("/active", (req, res) => {
	courseController.getActiveCourses(req.body).then(resultFromController => res.send(resultFromController));
})


//Route for retrieving a specific course
//Url: localhost:4000/courses/622515452125423415
router.get("/:courseId", (req, res) => {
	console.log(req.params)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));

})


// route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	const data = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedCourse: req.body
	}

	courseController.updateCourse(data).then(resultFromController => {
		res.send(resultFromController);
	})
})

// Instructions:
// 1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
// 2. Create a controller method for archiving a course obtaining the course ID from the request params
// 3. Process a PUT request at the /courseId/archive route using postman to archive a course
// 4. Push to git with the commit message of Add activity code - S35.
// 5. Add the link in Boodle.

router.put("/:courseId/archive", auth.verify, (req, res) => {
	const data = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	}

	courseController.archiveCourse(data).then(resultFromController => {
		res.send(resultFromController);
	})
})