const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");

const auth = require("../jwt/auth");

// Route for checking if email exists 
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})


// Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => {
		res.send(resultFromController);
	})
})

// Route for User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => {
		res.send(resultFromController);
	})
})


// ============1. Create a /details route that will accept the user’s Id to retrieve the details of a user.==============/
// Route for User Details
router.post("/details", (req, res) => {
	userController.retrieveUser(req.body).then(resultFromController => {
		res.send(resultFromController);
	})
})


router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.getProfile({userId: userData.id}).then(resultFromController => {
		res.send(resultFromController);
	})
})

module.exports = router;

/*

//==========================================SESSION 33 ACTIVITY ======================================================//
Activity:
1. Create a /details route that will accept the user’s Id to retrieve the details of a user.
2. Create a getProfile controller method for retrieving the details of the user:
a. Find the document in the database using the user's ID
b. Reassign the password of the returned document to an empty string
c. Return the result back to the frontend
3. Process a POST request at the /details route using postman to retrieve the details of the user.
4. Push to git with the commit message of Add activity code - S33.
5. Add the link in Boodle.

*/

router.post("/enroll", auth.verify, (req, res) => {


	let data = {
		courseId: req.body.courseId,
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.enroll(data).then(resultFromController => {
		res.send(resultFromController);
	})
})